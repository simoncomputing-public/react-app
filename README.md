# Simplified Arrival

To run:

```shell
npm install
```

| NPM Commands | Yarn Commands | Description |
| ------------ | ------------- | ----------- |
| npm start    | yarn start    | Starts server on http://localhost:3000 in dev mode, dynamically reloads on code change |
| npm test     | yarn test     | Starts Jest interactive testing |
| npm run build | yarn build | Builds the app for production in the ```build``` folder |