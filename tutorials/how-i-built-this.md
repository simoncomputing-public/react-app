# How I Built This

This project was started using the [Create React App](https://github.com/facebookincubator/create-react-app).
This is a summary of the tools and tutorials that were used to build this project.   This is mostly cut and paste and original tutorials are credited at the bottom of this page.

## Versions

This was built using the following versions of software:

1. npm 5.3.0
1. create-react-app 1.4.3
1. Visual Studio Code Version 1.17.2
    - ES Lint 1.4.3
1. material-ui 1.0.0-beta.21
1. material-ui-icons 1.0.0-beta.17
1. react-router 4.2.0

## Creating the application

To install create-react-app

```shell
npm install -g create-react-app
```

To create a new app, go to you **git** home directory and run:
```shell
create-react-app my-app
cd my-app
```

| NPM Commands | Yarn Commands | Description |
| ------------ | ------------- | ----------- |
| npm start    | yarn start    | Starts server on http://localhost:3000 in dev mode, dynamically reloads on code change |
| npm test     | yarn test     | Starts Jest interactive testing |
| npm run build | yarn build | Builds the app for production in the ```build``` folder |

## ES Lint for Visual Studio Code

ES Lint is very useful for keeping the code clean.  To install in Visual Studio Code click on the ```Extensions``` link from the command bar on the right (last icon) and search for **ES Lint**.

At the root of the project, create ```.eslintrc``` with the following content:

```json
{
  "extends": "react-app"
}
```
ES Lint will enforce standards in Visual Studio Code only.  Enforcing this any further is a team based decision.  This only works using npm version 3 or higher.

You'll also need to add this plugin:

```shell
npm install -g eslint-plugin-react
```

## Chrome Debugger Extension for Visual Studio Code

To install in Visual Studio Code click on the ```Extensions``` link from the command bar on the right (last icon) and search for **Debugger for Chrome**.

Then add the block below to your launch.json file and put it inside the .vscode folder in your app’s root directory.

```json
{
  "version": "0.2.0",
  "configurations": [{
    "name": "Chrome",
    "type": "chrome",
    "request": "launch",
    "url": "http://localhost:3000",
    "webRoot": "${workspaceRoot}/src",
    "userDataDir": "${workspaceRoot}/.vscode/chrome",
    "sourceMapPathOverrides": {
      "webpack:///src/*": "${webRoot}/*"
    }
  }]
}

```
| THe URL will be different if you changed the HOST or PORT environment variable.

Start your app by running npm start, and start debugging in VS Code by pressing F5 or by clicking the debug icon on the left-hand command bar.

## Installing Material Design

Install the **material-ui@next** as well as the **material-ui-icons** packages:

```shell
npm install material-ui@next material-ui-icons typeface-roboto

```
Add the following import to **index.js**:

```javascript
import 'typeface-roboto'
```

## Installing react-router

```shell
npm install react-router
```

## Installing Redux

```shell
npm install redux react-redux redux-form
npm install --save-dev redux-devtools deep-freeze
```

## Resources

### General

1. https://github.com/facebookincubator/create-react-app
1. https://blog.risingstack.com/react-js-best-practices-for-2016/
1. https://spring.io/guides/tutorials/react-and-spring-data-rest/

### Material Design Lite Information
1. https://stackoverflow.com/questions/31709013/material-design-lite-react-issues-with-tooltips

1. https://material-ui-next.com/
1. https://material-ui-next.com/getting-started/installation/

1. https://codesandbox.io/s/4j7m47vlm4?from-embed

### Router Information
1. https://reacttraining.com/react-router/
1. https://reacttraining.com/react-router/web/guides/philosophy
1. https://medium.com/@pshrmn/a-simple-react-router-v4-tutorial-7f23ff27adf

### Redux
1. https://github.com/reactjs/redux
1. https://egghead.io/series/getting-started-with-redux
1. https://egghead.io/lessons/react-redux-avoiding-object-mutations-with-object-assign-and-spread
    - Object.assign( {}, original, {completed: !todo.completed} );
    - { ...original, completed: !todo.completed }  - Babel stage 2 preset
1. https://redux-form.com/7.1.2/examples/material-ui/
```shell
npm install redux react-redux redux-form
npm install --save-dev redux-devtools deep-freeze
```

### Web Socket
1. https://spring.io/guides/gs/messaging-stomp-websocket/
1. https://html.spec.whatwg.org/multipage/web-sockets.html#network
1. https://codeburst.io/why-you-don-t-need-socket-io-6848f1c871cd

