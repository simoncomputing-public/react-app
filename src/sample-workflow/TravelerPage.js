import React from 'react';

import { withStyles, withTheme } from 'material-ui/styles';

import Paper from 'material-ui/Paper';
import Grid from 'material-ui/Grid';

import TravelerCard from '../components/TravelerCard';



const styles = theme => ({

    root: {
        flexGrow: 1,
        marginTop: 10,
    },
    paper: {
        marginLeft: 7,
        marginRight: 7,
        padding: 5,
        background: "#ffffff",
        height: theme.panel.height
    }
});
class TravelerPage extends React.Component {

    render = () => {
        const { theme, classes } = this.props;

        return (
            <div className={classes.root}>
                <Grid container spacing={16} alignItems="stretch">
                    <Grid item xs={5}>
                            <TravelerCard/>
                    </Grid>

                    <Grid item xs={7}>
                        <Paper className={classes.paper}>Stuff</Paper>
                    </Grid>
                    
                </Grid>

            </div>
        );
    }

}

export default withStyles(styles)(TravelerPage);