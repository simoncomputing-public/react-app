import React from 'react';
import { Field } from 'redux-form';

import MaskedInput from 'react-text-mask';
import NumberFormat from 'react-number-format';

import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Input from 'material-ui/Input';

class TextMaskCustom extends React.Component {
    render() {
      return (
        <MaskedInput
          {...this.props}
          mask={['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]}
          placeholderChar={'\u2000'}
          showMask
        />
      );
    }
  }
  
  class NumberFormatCustom extends React.Component {
    render() {
      return (
        <NumberFormat
          {...this.props}
          onValueChange={values => {
            this.props.onChange({
              target: {
                value: values.value,
              },
            });
          }}
          thousandSeparator
          prefix="$"
        />
      );
    }
  }
    
const renderMaskedField = ({
    input,
    label,
    className,
    placeHolderChar,
    disabled,
    showMask,
    meta: { touched, error },
    ...custom
}) => (
    <MaskedInput
        className={className}
        label={label}
        disabled={disabled}
        mask={['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]}
        placeHolderChar={placeHolderChar}
        showMask={showMask}
        {...input}
        {...custom}

    />
);
class MdMaskedField extends React.Component { 

    render() {
        const { name, label, className, disabled } = this.props;
        return (
            <Field name={name} label={label} className={className} component={renderMaskedField} disabled={disabled}/>
        );
    }

}

export default MdMaskedField;