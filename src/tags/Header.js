import React from 'react';

import { withStyles } from 'material-ui/styles';

import AppBar from 'material-ui/AppBar';
import Toolbar from 'material-ui/Toolbar';
import Typography from 'material-ui/Typography';
import MenuIcon from 'material-ui-icons/Menu';
import IconButton from 'material-ui/IconButton';
import { grey } from 'material-ui/colors';

import AccountCircle from 'material-ui-icons/AccountCircle';

//import dhsLogo from '../resources/dhs-logo.svg';

const styles = theme => ({
    logo: {
        height: "65px",
        marginRight: 25
    },
    title: {
        color: grey[50],
        fontWeight: 300
    },
    menuButton: {
        marginLeft: -12,
        marginRight: 20,
    },
    pushRight: {
        marginLeft: "auto",
    }
});

class Header extends React.Component {
    render = () => {
        const { classes } = this.props;

        return (
            <div>
                <AppBar position="static">
                    <Toolbar  >
                        <IconButton className={classes.menuButton} color="contrast" aria-label="Menu">
                            <MenuIcon />
                        </IconButton>

                        <Typography type="title" className={classes.title}>
                            Simplified Arrival
                    </Typography>
                        <div className={classes.pushRight}>
                            <IconButton color="contrast">
                                <AccountCircle />
                            </IconButton>
                        </div>

                </Toolbar>
            </AppBar>

            </div >
        );
    }
}

export default withStyles(styles)(Header);

