import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import TextField from 'material-ui/TextField';

const styles = theme => ({
    container: {
      display: 'flex',
      flexWrap: 'wrap',
    },
    textField: {
      marginLeft: theme.spacing.unit,
      marginRight: theme.spacing.unit,
      width: 200,
    },
  });
  
  function DatePickers(props) {
    const { classes } = props;
  
    return (
        <TextField
          id="date"
          label="DOB"
          type="date"
          defaultValue="2017-05-24"
          className={classes.textField}
          InputLabelProps={{
            shrink: true,
          }}
        />
    );
  }
  
  DatePickers.propTypes = {
    classes: PropTypes.object.isRequired,
  };

  export default withStyles(styles)(DatePicker);
  
  