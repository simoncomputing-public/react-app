import React, { Component } from 'react';
import { Field } from 'redux-form';

import Input, { InputLabel } from 'material-ui/Input';
import { FormControl, FormHelperText } from 'material-ui/Form';
import Select from 'material-ui/Select';

const renderSelectField = ({
    input,
    label,
    className,
    meta: { touched, error, form },
    children,
    ...custom
}) => (
    <FormControl className={className} error={error}>
        <InputLabel htmlFor={form + '.' + input.name}>{label}</InputLabel>
        <Select
            native
            value={input.value}
            input={<Input id={form + '.' + input.name}/>}
            onChange={value=>input.onChange(value)}
        >
            {children}
        </Select>
        <FormHelperText>{error}</FormHelperText>
    </FormControl>
);


/**
 * Intended Use:
 * <MdSelect name="role" label="User Role">
 *      <option value="" />
 *      <option value="Admin" />
 *      <option value="PowerUser" />
 *      <option value="Developer" />
 * </MdSelect>
 */
class MdSelect extends Component {
    render() {
        const { name, label, className, children } = this.props;
        return (
            <Field name={name} label={label} className={className} component={renderSelectField}>
                {children}
            </Field>
        );
    }
}

export default MdSelect;

