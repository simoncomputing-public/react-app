import React, { Component } from 'react';
import { Field } from 'redux-form';
import TextField from 'material-ui/TextField';

const renderTextField = ({
    input,
    label,
    className,
    disabled,
    type,
    meta: { touched, error },
    ...custom
}) => (
    <TextField
        className={className}
        label={label}
        type={type}
        disabled={disabled}
        {...input}
        {...custom}
    />
);

class MdTextField extends Component {

    render() {
        const { name, label, className, disabled, type } = this.props;
        return (
            <Field name={name} label={label} className={className} component={renderTextField} type={type} disabled={disabled}/>
        );
    }

}

export default MdTextField;
