import { createStore, combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';


const rootReducer = combineReducers({
    // ...your other reducers here
    // you have to pass formReducer under 'form' key,
    // for custom keys look up the docs for 'getFormState'
    auth: authReducer,
    form: formReducer
});

/**
 * Authorization/Authentication State
 *
 * @param {*} state - userId, email, regions[], roles[]
 * @param {*} action
 */
function authReducer( state = {}, action ) {
    switch( action.type ) {
        case 'LOGON':
            return { ...state, ...action.state };
        case 'LOGOUT':
        default:
            return {}
    }
}

const store = createStore(rootReducer);

export default store;
