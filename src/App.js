import React, { Component } from 'react';
import { Provider } from 'react-redux';
import './App.css';

// Theme support
import { MuiThemeProvider, createMuiTheme } from 'material-ui/styles';
import indigo from 'material-ui/colors/indigo';
import red from 'material-ui/colors/red';
import blueGrey from 'material-ui/colors/blueGrey';

import Header from './tags/Header';
import TravelerPage from './sample-workflow/TravelerPage';

import store from './reducers/reducers';

// Customized Theme
const theme = createMuiTheme({
  panel: {
    height: 700,
  },
  palette: {
    primary: indigo,
    secondary: blueGrey,
    error: red
  }
});


class App extends Component {
  render() {

    return (
      <Provider store={store}>
        <MuiThemeProvider theme={theme}>
          <Header />
          <TravelerPage />
        </MuiThemeProvider>
      </Provider>
    );
  }
}

export default App;
