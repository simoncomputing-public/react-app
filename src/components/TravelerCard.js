import React from 'react';

import PropTypes from 'prop-types';

import { withStyles } from 'material-ui/styles';
import { yellow } from 'material-ui/colors';
import Card, { CardHeader, CardMedia, CardContent, CardActions } from 'material-ui/Card';

import Avatar from 'material-ui/Avatar';
import IconButton from 'material-ui/IconButton';
import Typography from 'material-ui/Typography';
import Grid from 'material-ui/Grid';

import MdTextField from '../tags/MdTextField';
import MdMaskedField from '../tags/MdMaskedField';
import dhsLogo from '../resources/dhs-logo.svg';

import {  reduxForm } from 'redux-form';

const styles = theme => ({

    root: {
        flexGrow: 1,
        marginTop: 10,
    },
    card: {
        marginLeft: 7,
        maxWidth: 400,
        height: theme.panel.height,
    },
    media: {
        width: 150,
        display: "block",
        backgroundColor: "grey",
        marginLeft: "auto",
        marginRight: "auto"
    },
    avatar: {
        backgroundColor: yellow[500],
        color: "black"
    },
    cardColumn: {
        width: 200,
    },
    textField: {
        marginTop: theme.spacing.unit * 2,
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: 160,
    },

});


class TravelerCard extends React.Component {
    componentDidMount = () => {
        this.handleInitialize();
    }

    handleInitialize = () => {
        const { initialize } = this.props;

        initialize({ 
            name: "HA, SUNG HUYN", 
            dob: "01/01/1980",
            nationality: "KOR",
            docNo: "241359677",
            travelerType: "Visitor", 
            gender: "Male",
            cob: "KOR",
            carrier: "AA"
        });
        
    }    
    render = () => {
        const { classes } = this.props;

        return (
            <div>
                <Card className={classes.card}>
                    <CardHeader
                        avatar={
                            <Avatar aria-label="Unprocessed" className={classes.avatar}>
                                U
                            </Avatar>
                        }
                        title="Passport"
                        subheader="KOR Korea"
                    />
                    <div className={classes.root}>
                        <form>
                        <Grid container spacing={16}>
                            <Grid item xs className={classes.cardColumn}>
                                <img
                                    className={classes.media}
                                    src={dhsLogo}
                                    title="Image place holder"
                                />
                                <br/>
                                <MdTextField name="name" label="Name" fullWidth="true" className={classes.textField}/><br />
                                <MdTextField name="cob" label="COB" className={classes.textField}/><br />
                                <MdTextField name="nationality" label="Nationality" className={classes.textField}/><br />
                                <MdTextField name="docNo" label="Document No." className={classes.textField}/><br />

                            </Grid>
                            <Grid item xs className={classes.cardColumn}>
                                <img
                                    className={classes.media}
                                    src={dhsLogo}
                                    title="Image place holder"
                                />
                                <br/>fs
                                <MdTextField name="travelerType" label="Traveler Type" fullWidth="true" className={classes.textField}/><br />
                                <MdTextField name="gender" label="Gender" className={classes.textField}/><br />
                                <MdTextField name="nationality" label="Nationality" className={classes.textField}/><br />
                                <MdTextField name="carrier" label="Carrier" className={classes.textField}/><br />
                            </Grid>
                        </Grid>
                        </form>
                    </div>

                </Card>
            </div>
        );

    }

}

export default reduxForm({ form: 'userEntry' })(
    withStyles(styles)(TravelerCard)
);